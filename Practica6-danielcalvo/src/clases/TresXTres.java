package clases;

public class TresXTres extends Cubito{
	
	private int nLetasScramble3x3;
	
	public TresXTres() {
		super();
		this.nLetasScramble3x3=0;
	}
	
	public TresXTres(String idCubito, String marca, String modelo, 
			double precioMercado, String tiempo, boolean stickers, int nLetasScramble3x3) {
		super(idCubito, marca, modelo, precioMercado, tiempo, stickers);
		this.nLetasScramble3x3=nLetasScramble3x3;
	}
	

	public int getnLetasScramble3x3() {
		return nLetasScramble3x3;
	}

	public void setnLetasScramble3x3(int nLetasScramble3x3) {
		this.nLetasScramble3x3 = nLetasScramble3x3;
	}

	@Override
	public String toString() {
		return "TresXTres [nLetasScramble3x3=" + nLetasScramble3x3 + ", toString()=" + super.toString() + "]";
	}

	
	
	
	
}
