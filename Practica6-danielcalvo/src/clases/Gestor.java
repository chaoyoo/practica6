package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Gestor {
	public Scanner lector = new Scanner(System.in);
	private ArrayList<Speedcuber> listaSpeedcubers;
	private ArrayList<Cubito> listaCubitos;
	
	public Gestor() {
		listaSpeedcubers=new ArrayList<Speedcuber>();
		listaCubitos=new ArrayList<Cubito>();
	}
	
	public void altaSpeedcuber(String idSpeedcuber, String nombre, String nacionalidad, 
			int wins, String fechaNacimiento) {
		for(Speedcuber speedcuber : listaSpeedcubers) {
			if(speedcuber!=null) {
				Speedcuber nuevoSpeedcuber = new Speedcuber(idSpeedcuber, nombre, nacionalidad, wins);
				nuevoSpeedcuber.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
				listaSpeedcubers.add(nuevoSpeedcuber);
				System.out.println("Speedcuber a�adido");
			}
		}
	}
	
	public void listarSpeedcubers() {
		for(Speedcuber speedcuber: listaSpeedcubers) {
			if(speedcuber!=null) {
		System.out.println(speedcuber);
			}
		}
	}
	
	public Speedcuber buscarSpeedcuber(String idSpeedcuber) {
		for(Speedcuber speedcuber: listaSpeedcubers) {
			if(speedcuber!=null && speedcuber.getIdSpeedcuber().equals(idSpeedcuber)) {
				return speedcuber;
			}
		}
		return null;
	}
	
	public void eliminarSpeedcuber(String idSpeedcuber) {
		Iterator<Speedcuber> iteradorSpeedcubers = listaSpeedcubers.iterator();
		while(iteradorSpeedcubers.hasNext()) {
			Speedcuber speedcuber = iteradorSpeedcubers.next();
			if(speedcuber.getIdSpeedcuber().equals(idSpeedcuber)) {
				iteradorSpeedcubers.remove();
			}
		}
	}
	
	public void altaCubito(String idCubito, String marca, String modelo, 
			double precioMercado, String tiempo, boolean stickers) {
		for(Cubito cubito: listaCubitos) {
			if(cubito!=null) {
				Cubito miCubito = new Cubito(idCubito, marca, modelo, precioMercado, tiempo, stickers);
				listaCubitos.add(miCubito);
				System.out.println("Cubito a�adido");
			}
		}
	}
	
	public void altaTresXTres(String idCubito, String marca, String modelo, 
			double precioMercado, String tiempo, boolean stickers, int nLetrasScramble3x3) {
		for(Cubito cubitos:listaCubitos) {
			if(cubitos!=null) {
				TresXTres mitresxtres = new TresXTres(idCubito, marca, modelo, precioMercado, tiempo, stickers, nLetrasScramble3x3);
				listaCubitos.add(mitresxtres);
				System.out.println("Cubito a�adido");
			}
		}
	}
	
	public void altaDosXDos(String idCubito, String marca, String modelo, 
			double precioMercado, String tiempo, boolean stickers, int nLetrasScramble2x2) {
		for(Cubito cubitos:listaCubitos) {
			if(cubitos!=null) {
				DosXDos midosxdos = new DosXDos(idCubito, marca, modelo, precioMercado, tiempo, stickers, nLetrasScramble2x2);
				listaCubitos.add(midosxdos);
				System.out.println("Cubito a�adido");
			}
		}
	}
	
	public void listarCubitos() {
		for(Cubito cubitos: listaCubitos) {
			if(cubitos!=null) {
				System.out.println(cubitos);
			}
		}
		
	}
	
	public Cubito buscarCubito(String idCubito) {
		for(Cubito cubitos: listaCubitos) {
			if(cubitos!=null && cubitos.getIdCubito().equals(idCubito)) {
				return cubitos;
			}
		}
		return null;
	}
	
	public void eliminarCubito(String idCubito) {
		Iterator<Cubito> iteradorCubitos = listaCubitos.iterator();
		while(iteradorCubitos.hasNext()) {
			Cubito cubito = iteradorCubitos.next();
			if(cubito.getIdCubito().equals(idCubito)) {
				iteradorCubitos.remove();
				System.out.println("Cubito eliminado");
			}
		}
	}
	
	public void asignarCubito(String idSpeedcuber, String idCubito) {
		Speedcuber speedcuber = buscarSpeedcuber(idSpeedcuber);
		Cubito cubito = buscarCubito(idCubito);
		speedcuber.setCubito(cubito);
	}
	
	public void listarSpeedcubersPorCubito(String marca, String modelo) {
		for(Speedcuber speedcuber: listaSpeedcubers) {
			if(speedcuber.getCubito()!=null && speedcuber.getCubito().equals(marca) && speedcuber.getCubito().equals(modelo)) {
				System.out.println(speedcuber);
			} else {
				System.out.println("Los campos no coinciden o no hay ning�n speedcuber que tenga ese cubito");
			}
		}
	}
	
	public void nivel3x3() {
		
		System.out.println("Seg�n tu record en el 3x3 te dir� el nivel que tienes");
		System.out.println("1. +3 mins");
		System.out.println("2. 3-2 mins");
		System.out.println("3. 2-1 mins");
		System.out.println("4. sub 1 min");
		System.out.println("5. 50-40 seg");
		System.out.println("6. 40-30 seg");
		System.out.println("7. 30-20 seg");
		System.out.println("8. 20-10 seg");
		System.out.println("9. sub 10 seg");
		System.out.println("10. sub 5 seg");
		int opcion=lector.nextInt();
		switch(opcion) {
		case 1:
			System.out.println("Sin mentirme, �has hecho el cubo m�s de una vez? "
					+ "Necesitas resolverlo m�s veces.");
			break;
			
		case 2:
			System.out.println("Probablemente hayas aprendido hace no mucho, "
					+ "practica todos los dias un poco y ver�s como los tiempos bajan.");
			break;
			
		case 3:
			System.out.println("�Todavia no te has cansado? Ya no hay vuelta atras, "
					+ "te espera una vida haciendo cubitos");
			break;
			
		case 4:
			System.out.println("�Enhorabuena por el sub 1 minuto! Seguramente ya medio domines los finger-tricks");
			break;
			
		case 5:
			System.out.println("Creo que es momento de que aprendas otros m�todos para bajar tiempos, "
					+ "no vas mal, pero te falta practica para ir a Got Talent");
			break;
			
		case 6:
			System.out.println("A estas alturas tienes m�s de 10 cubos diferentes en la estanter�a, �miento?");
			break;
			
		case 7:
			System.out.println("Eres realmente r�pido, pero te quedan muchos algoritmos por memorizar");
			break;
			
		case 8:
			System.out.println("Merece mucho la pena que te apuntes a alguna competici�n");
			break;
			
		case 9:
			System.out.println("Siento decirte que has llegado al punto m�ximo del speedcubing, �sigue asi!");
			break;
			
		case 10:
			System.out.println("Mientes, o simplemente eres Max Park");
			break;
			
		default:
			System.out.println("Introduce un numero entre el 1 y el 10 la proxima vez");
			break;
		}
		System.out.println("No pierdas la costumbre de hacer cubos porfa");
		
	}
	
public void nivel2x2() {
		
		System.out.println("Seg�n tu record en el 2x2 te dir� el nivel que tienes");
		System.out.println("1. +2 mins");
		System.out.println("2. 2-1 mins");
		System.out.println("3. sub 1 min");
		System.out.println("4. 40-20 seg");
		System.out.println("5. 20-10 seg");
		System.out.println("6. sub 10 seg");
		System.out.println("7. sub 5 seg");
		System.out.println("8. sub 1 seg");
		int opcion=lector.nextInt();
		switch(opcion) {
		case 1:
			System.out.println("Sin mentirme, �has hecho el cubo m�s de una vez? "
					+ "Necesitas resolverlo m�s veces.");
			break;
			
		case 2:
			System.out.println("Probablemente hayas aprendido hace no mucho, "
					+ "practica todos los dias un poco y ver�s como los tiempos bajan.");
			break;
			
		case 3:
			System.out.println("�Todavia no te has cansado? Ya no hay vuelta atras, "
					+ "te espera una vida haciendo cubitos");
			break;
			
		case 4:
			System.out.println("Seguramente ya medio domines los finger-tricks");
			break;
			
		case 5:
			System.out.println("No vas mal, pero te falta practica para ir a Got Talent");
			break;
			
		case 6:
			System.out.println("A estas alturas tienes m�s de 10 cubos diferentes en la estanter�a, �miento?");
			break;
			
		case 7:
			System.out.println("�Eres realmente r�pido!");
			break;
			
		case 8:
			System.out.println("Muy muy pocos tienen esa habilidad");
			break;
			
		default:
			System.out.println("Introduce un numero entre el 1 y el 8 la proxima vez");
			break;
		}
		System.out.println("No pierdas la costumbre de hacer cubos porfa");
		
	}
	public void setDNF(String tiempo) {
		for(Cubito cubito: listaCubitos) {
			if(cubito!=null && cubito.getTiempo().equals(tiempo)) {
				cubito.setTiempo("DNF");
				System.out.println("Tiempo cambiado");
			} else {
				System.out.println("El tiempo no existe");
			}
		}
	}
	
	public void infoTresXTres() {
		System.out.println("El cubo de rubik es un rompecabezas mec�nico tradicional que consiste "
				+ "en un cubo de seis caras, cada una de sus seis caras est� cubierta por nueve pegatinas "
				+ "que son trozo de papel pl�stico de seis colores que son el blanco, rojo, azul, naranja, "
				+ "verde y amarillo, estos colores pueden variar porque cada una de sus piezas centrales "
				+ "muestran una cara de un solo color, doce piezas aristas, que es el segmento de recta "
				+ "donde se encuentran dos caras coloreadas y ocho piezas verticales que es el punto donde "
				+ "se encuentra tres caras coloreadas, Adem�s existe una versi�n especial para dalt�nicos "
				+ "que es un defecto que consiste en no distinguir los colores de las cuatro caras.");
	}
	
	public void infoDosXDos() {
		System.out.println("El 2x2 (tambi�n conocido como Minicubo, Mini Rubik o Cubo de hielo) es "
				+ "el equivalente de un Cubo de Rubik pero de dimensi�n 2x2x2. Aunque mec�nicamente "
				+ "es m�s complejo que el Cubo de Rubik, su resoluci�n es mucho m�s sencilla, "
				+ "ya que es an�loga a reordenar �nicamente los v�rtices de un cubo de Rubik est�ndar 3x3x3.");
	}
	
	public void personalizarCubito(String idCubito) {
		for(Cubito cubito: listaCubitos) {
			if(cubito!=null && cubito.getIdCubito().equals(idCubito)) {
				cubito.setStickers(true);
				System.out.println("Cubito personalizado");
			} else {
				System.out.println("El cubito no existe");
			}
		}
	}
}
