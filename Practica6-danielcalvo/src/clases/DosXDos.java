package clases;

public class DosXDos extends Cubito{

	private int nLetrasScramble2x2;
	
	public DosXDos() {
		super();
		this.nLetrasScramble2x2=0;
	}
	
	public DosXDos(String idCubito, String marca, String modelo, 
			double precioMercado, String tiempo, boolean stickers, int nLetrasScramble2x2) {
		super(idCubito, marca, modelo, precioMercado, tiempo, stickers);
		this.nLetrasScramble2x2=nLetrasScramble2x2;
	}

	
	public int getnLetrasScramble2x2() {
		return nLetrasScramble2x2;
	}

	public void setnLetrasScramble2x2(int nLetrasScramble2x2) {
		this.nLetrasScramble2x2 = nLetrasScramble2x2;
	}

	@Override
	public String toString() {
		return "DosXDos [toString()=" + super.toString() + "]";
	}

	
	
	
}