package clases;

public class Cubito {

	private String idCubito;
	private String marca;
	private String modelo;
	private double precioMercado;
	private String tiempo;
	private boolean stickers;
	
	public Cubito() {
		this.idCubito="";
		this.marca="";
		this.modelo="";
		this.precioMercado=0;
		this.tiempo="";
		this.stickers=false;
	}
	
	public Cubito(String idCubito, String marca, String modelo, double precioMercado, String tiempo, boolean stickers) {
		this.idCubito=idCubito;
		this.marca=marca;
		this.modelo=modelo;
		this.precioMercado=precioMercado;
		this.tiempo=tiempo;
		this.stickers=false;
	}
	
	public String getIdCubito() {
		return idCubito;
	}
	public void setIdCubito(String idCubito) {
		this.idCubito = idCubito;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public double getPrecioMercado() {
		return precioMercado;
	}
	public void setPrecioMercado(double precioMercado) {
		this.precioMercado = precioMercado;
	}
	public String getTiempo() {
		return tiempo;
	}
	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}
	
	public boolean isStickers() {
		return stickers;
	}

	public void setStickers(boolean stickers) {
		this.stickers = stickers;
	}

	@Override
	public String toString() {
		return "Cubito [idCubito=" + idCubito + ", marca=" + marca + ", modelo=" + modelo + ", precioMercado="
				+ precioMercado + ", tiempo=" + tiempo + ", stickers=" + stickers + "]";
	}
	
	

	
	
	
}
