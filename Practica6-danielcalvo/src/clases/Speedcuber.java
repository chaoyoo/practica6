package clases;

import java.time.LocalDate;

public class Speedcuber {

	private String idSpeedcuber;
	private String nombre;
	private String nacionalidad;
	private int wins;
	private LocalDate fechaNacimiento;
	private Cubito cubito;
	
	public Speedcuber() {
	this.idSpeedcuber="";
	this.nombre="";
	this.nacionalidad="";
	this.wins=0;
	}
	
	public Speedcuber(String id, String nombre, String nacionalidad, 
			int wins) {
		this.idSpeedcuber=id;
		this.nombre=nombre;
		this.nacionalidad=nacionalidad;
		this.wins=wins;
		
	}
	
	public String getIdSpeedcuber() {
		return idSpeedcuber;
	}

	public void setIdSpeedcuber(String idSpeedcuber) {
		this.idSpeedcuber = idSpeedcuber;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public int getWins() {
		return wins;
	}
	public void setWins(int wins) {
		this.wins = wins;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public Cubito getCubito() {
		return cubito;
	}

	public void setCubito(Cubito cubito) {
		this.cubito = cubito;
	}

	@Override
	public String toString() {
		return "Speedcuber [idSpeedcuber=" + idSpeedcuber + ", nombre=" + nombre + ", nacionalidad=" + nacionalidad
				+ ", wins=" + wins + ", fechaNacimiento=" + fechaNacimiento + ", cubito=" + cubito + "]";
	}

	
	
	
}
