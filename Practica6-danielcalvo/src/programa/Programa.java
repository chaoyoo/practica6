package programa;

import java.util.Scanner;

import clases.Gestor;

public class Programa {
	static Scanner lector = new Scanner(System.in);
	public static void main(String[] args) {
		
		Gestor competicion = new Gestor();
		int opcion;
		do {
			System.out.println("Para salir selecciona el 0.");
			System.out.println("~~MENU~~");
			System.out.println("1. Dar de alta speedcuber");
			System.out.println("2. Buscar speedcuber");
			System.out.println("3. Listar speedcubers");
			System.out.println("4. Eliminar speedcuber");
			System.out.println("5. Dar de alta cubito");
			System.out.println("8. Buscar cubito");
			System.out.println("9. Listar cubitos");
			System.out.println("10. Eliminar cubito");
			System.out.println("11. Asignar cubito a speedcuber");
			System.out.println("12. Listar los speedcubers que tienen el mismo cubito");
			System.out.println("13. Nivel 3x3");
			System.out.println("14. Nivel 2x2");
			System.out.println("15. Modificar tiempo a DNF");
			System.out.println("16. Info 3x3");
			System.out.println("17. Info 2x2");
			System.out.println("18. Personalizar cubito (stickers)");
			opcion=lector.nextInt();
			switch(opcion) {
			case 0:
				System.out.println("Adi�s!");
				break;
				
			case 1:
				lector.nextLine();
				System.out.println("Has seleccionado dar de alta speedcuber");
				System.out.println("Dame su id");
				String idSpeedcuber=lector.nextLine();
				System.out.println("Dame su nombre");
				String nombre=lector.nextLine();
				System.out.println("Dame su nacionalidad");
				String nacionalidad=lector.nextLine();
				System.out.println("Inserte su fecha de nacimiento");
				String fechaNacimiento=lector.nextLine();
				System.out.println("Inserte cuantas competiciones ha ganado");
				int wins=lector.nextInt();
				competicion.altaSpeedcuber(idSpeedcuber, nombre, nacionalidad, wins, fechaNacimiento);
				break;
					
			case 2: 
				lector.nextLine();
				System.out.println("Has seleccionado buscar un speedcuber");
				System.out.println("Dame su id");
				idSpeedcuber=lector.nextLine();
				competicion.buscarSpeedcuber(idSpeedcuber);
				break;
				
			case 3:
				lector.nextLine();
				System.out.println("Has seleccionado listar speedcubers");
				competicion.listarSpeedcubers();
				break;
				
			case 4:
				lector.nextLine();
				System.out.println("Has seleccionado eliminar speedcuber");
				System.out.println("Dame su id");
				idSpeedcuber=lector.nextLine();
				competicion.eliminarSpeedcuber(idSpeedcuber);
				System.out.println("Speedcuber eliminado");
				break;
				
			case 5:
				lector.nextLine();
				System.out.println("Has seleccionado dar de alta cubito");
				System.out.println("Dame el id del cubito");
				String idCubito=lector.nextLine();
				System.out.println("Dame la marca");
				String marca = lector.nextLine();
				System.out.println("Dame el modelo");
				String modelo = lector.nextLine();
				System.out.println("Dame tu r�cord");
				String tiempo = lector.nextLine();
				System.out.println("�Cu�nto te cost�?");
				double precioMercado = lector.nextDouble();
				boolean stickers = false;
				competicion.altaCubito(idCubito, marca, modelo, precioMercado, tiempo, stickers);
				break;
				
			case 6:
				lector.nextLine();
				System.out.println("Has seleccionado buscar cubito");
				System.out.println("Dame su id");
				idCubito=lector.nextLine();
				competicion.buscarCubito(idCubito);
				break;
			
			case 7:
				lector.nextLine();
				System.out.println("Has seleccionado dar de alta 3x3");
				System.out.println("Dame el id del cubito");
				idCubito=lector.nextLine();
				System.out.println("Dame la marca");
				marca = lector.nextLine();
				System.out.println("Dame el modelo");
				modelo = lector.nextLine();
				System.out.println("Dame tu r�cord");
				tiempo = lector.nextLine();
				System.out.println("�Cu�nto te cost�?");
				precioMercado = lector.nextDouble();
				stickers = false;
				System.out.println("Dame el numero de letras del scramble");
				int nLetrasScramble3x3 = lector.nextInt();
				competicion.altaTresXTres(idCubito, marca, modelo, precioMercado, tiempo, stickers, nLetrasScramble3x3);
				break;
				
			case 8:
				lector.nextLine();
				System.out.println("Has seleccionado dar de alta 2x2");
				System.out.println("Dame el id del cubito");
				idCubito=lector.nextLine();
				System.out.println("Dame la marca");
				marca = lector.nextLine();
				System.out.println("Dame el modelo");
				modelo = lector.nextLine();
				System.out.println("Dame tu r�cord");
				tiempo = lector.nextLine();
				System.out.println("�Cu�nto te cost�?");
				precioMercado = lector.nextDouble();
				stickers = false;
				System.out.println("Dame el numero de letras del scramble");
				int nLetrasScramble2x2 = lector.nextInt();
				competicion.altaDosXDos(idCubito, marca, modelo, precioMercado, tiempo, stickers, nLetrasScramble2x2);
				break;
				
			case 9:
				System.out.println("Has seleccionado listar cubitos");
				competicion.listarCubitos();
				break;
				
			case 10:
				lector.nextLine();
				System.out.println("Has seleccionado eliminar un cubito");
				System.out.println("Dame su id");
				idCubito=lector.nextLine();
				competicion.eliminarCubito(idCubito);
				break;
				
			case 11:
				lector.nextLine();
				System.out.println("Has seleccionado asignar cubito a speedcuber");
				System.out.println("Dame la id del speedcuber");
				idSpeedcuber=lector.nextLine();
				System.out.println("Dame la id del cubito");
				idCubito=lector.nextLine();
				competicion.asignarCubito(idSpeedcuber, idCubito);
				break;
				
			case 12:
				lector.nextLine();
				System.out.println("Has seleccionado listar los speedcubers que tienen un cubito en concreto");
				System.out.println("Dame la marca del cubito");
				marca=lector.nextLine();
				System.out.println("Dame el modelo");
				modelo=lector.nextLine();
				competicion.listarSpeedcubersPorCubito(marca, modelo);
				break;
				
			case 13:
				lector.nextLine();
				System.out.println("Has seleccionado nivel 3x3");
				competicion.nivel3x3();
				break;
				
			case 14:
				lector.nextLine();
				System.out.println("Has seleccionado nivel 2x2");
				competicion.nivel2x2();
				break;
				
			case 15:
				lector.nextLine();
				System.out.println("Has seleccionado cambiar tiempo a DNF");
				System.out.println("Dame el tiempo");
				tiempo=lector.nextLine();
				competicion.setDNF(tiempo);
				break;
				
			case 16:
				competicion.infoTresXTres();
				break;
				
			case 17:
				competicion.infoDosXDos();
				break;
				
			case 18:
				System.out.println("Has seleccionado personalizar cubito");
				System.out.println("Introduce la id del cubito");
				idCubito=lector.nextLine();
				competicion.personalizarCubito(idCubito);
				break;
				
			default:
				System.out.println("Introduce un numero del 1 al 16");
				break;
			}
		}while(opcion!=0);

	}

}
